from html import escape
from re import search, findall


class HtmlGenerator:
    def __init__(self, data):
        """
        Генерирует html по данным json
        :param data:
        """
        self.elements = []
        self.load(data)

    def load(self, data):
        """
        Загружает список объектов в генератор
        :param data:
        :return:
        """
        if isinstance(data, list):
            self.elements.append(Element({'ul': data}))
        elif isinstance(data, dict):
            self.elements.append(Element(data))
        else:
            raise Exception("data must be type of list or dict")

    def get_html(self):
        """
        Отдает html содержимое объектов в строке
        :return:
        """
        html = ''
        for element in self.elements:
            html += element.to_string()
        return html


class Element:
    def __init__(self, data):
        """
        Конкретный html объект. Содержит все тэги одного уровня вложенности
        :param data:
        """
        self.tags = {}
        self.tags = self.load(data)

    @staticmethod
    def load(data):
        """
        Создает рекурсивно объекты, загружает данные из словаря.
        Создает html список из стандартного списка принудительно.
        :param data:
        :return:
        """
        tags = {}

        for tag, content in data.items():
            if isinstance(content, str):
                content = escape(content)

            if isinstance(content, list):
                list_elements = []
                for element_data in content:
                    if type(element_data) is Element:
                        list_elements.append(element_data)
                    else:
                        list_elements.append(Element({'li': element_data}))

                if tag == 'ul':
                    tags[tag] = list_elements
                else:
                    tags[tag] = Element({'ul': list_elements})

            elif isinstance(content, dict):
                tags[tag] = Element(content)
            else:
                tags[tag] = content
        return tags

    def to_string(self):
        """
        преобразует объект в строку html, рекурсивно
        :return:
        """
        html = ''
        for tag, content in self.tags.items():
            if isinstance(content, list):
                sub_html = ''
                for element in content:
                    sub_html += element.to_string()
                html += Wrapper.wrap_by_tag(sub_html, tag)
            elif isinstance(content, Element):
                html += Wrapper.wrap_by_tag(content.to_string(), tag)
            else:
                html += Wrapper.wrap_by_tag(content, tag)

        return html


class Wrapper:
    @staticmethod
    def wrap_by_tag(content, tag):
        """
        Создает html тэг с содержимым и мета данными
        :param content:
        :param tag:
        :return:
        """
        meta = Wrapper.parse_meta(tag)
        meta['content'] = content
        main = '<{tag} id="{id}" class="{class}" >{content}</{tag}>'
        main = main.format(**meta)

        return main

    @staticmethod
    def parse_meta(tag):
        """
        Парсит метаданные из названия тэга
        :param tag:
        :return:
        """
        tag_name = Wrapper.parse_tag(tag)
        id = Wrapper.parse_id_from_tag(tag)
        classes = Wrapper.parse_classes(tag)
        return {
            'tag': tag_name,
            'id': id,
            'class': classes
        }

    @staticmethod
    def parse_classes(tag):
        """
        Парсит конкретно список классов у тэга
        :param tag:
        :return:
        """
        result = Wrapper.search_in_string(tag, '[.][a-zA-Z0-9-]*', 0, True)
        cleared_classes = ''
        if type(result) is list:
            cleared_classes = cleared_classes.join(result)
        else:
            cleared_classes = result

        return cleared_classes.replace('.', ' ').strip()

    @staticmethod
    def parse_id_from_tag(tag):
        """
        Парсит идентификатор тэга
        :param tag:
        :return:
        """
        result = Wrapper.search_in_string(tag, '#[a-zA-Z0-9/-]*')
        return result.replace('#', '')

    @staticmethod
    def parse_tag(tag):
        """
        Выделяет название тэга
        :param tag:
        :return:
        """
        return Wrapper.search_in_string(tag, '^[a-zA-Z]*')

    @staticmethod
    def search_in_string(string, regex, group=0, all=False):
        """
        Обертка поверх re.search и re.findall
        :param string:
        :param regex:
        :param group:
        :param all:
        :return:
        """
        try:
            if not all:
                found = search(regex, string).group(group)
            else:
                found = findall(regex, string)
        except AttributeError:
            found = ''

        return found
