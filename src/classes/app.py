import os
import json
from .generator import HtmlGenerator


class Application:
    static_path = 'static/'

    def run(self, json_path=None):
        """
        Основной входной метод
        :return:
        """
        if type(json_path) is None:
            raise FileNotFoundError()

        path = self.make_path(json_path)
        data = self.get_data_from_file(path)
        generator = HtmlGenerator(data)
        html = generator.get_html()

        print(html)

    @staticmethod
    def get_data_from_file(path):
        """
        Загружает данные из *.json файла
        :param path: - путь до файла
        :return : dict|list
        """
        if not os.path.isfile(path):
            raise FileNotFoundError()
        with open(path, encoding='utf-8') as json_file:
            json_data = json.load(json_file)

        return json_data

    def make_path(self, file_name):
        """
        :param file_name: имя json файла
        :return:
        """
        static_path = self.static_path
        path = os.path.join(static_path, file_name)
        return path
